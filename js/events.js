/**
 * Created by emmetdonnelly on 15/08/2016.
 */


(function(){
    var didScroll = false;
    if(!$('#bannerDot').hasClass('w--current')){
        $('#bannerDot').addClass("w--current");
    }

    document.addEventListener("scroll", onBodyScroll);

    function onBodyScroll() {
        didScroll = true;
    }

    setInterval(function() {
        if(didScroll) {
            didScroll = false;
            selectDot();
        }
    }, 100);

    function selectDot() {

        var banner =  document.getElementById("banner");
        var intro = document.getElementById("intro");
        var screenshots = document.getElementById("screenshots");
        var testimonials = document.getElementById("testimonials");
        var contest = document.getElementById("contest");

        if(isElementInViewport(banner)){
            clearDots();
            $('#bannerDot').addClass("w--current");
        }
        else if(isElementInViewport(intro)){
            clearDots();
            $('#introDot').addClass("w--current");
        }
        else if(isElementInViewport(screenshots)){
            clearDots();
            $('#screenshotsDot').addClass("w--current");
        }
        else if(isElementInViewport(testimonials)){
            clearDots();
            $('#testimonialsDot').addClass("w--current");
        }
        else if(isElementInViewport(contest)){
            clearDots();
            $('#contestDot').addClass("w--current");
        }

    }

    function isElementInViewport (el) {
        var rect = el.getBoundingClientRect();

        //console.log(rect.top + " : " + rect.left + " : " + rect.bottom + " : " + rect.right);
        return (
            rect.bottom >= 50 &&
            rect.right >= 50
            // rect.top >= 0 &&
            // rect.left >= 0 &&
            // rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
            // rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );
    }

    function clearDots(){
        $('#bannerDot').removeClass("w--current");
        $('#introDot').removeClass("w--current");
        $('#screenshotsDot').removeClass("w--current");
        $('#testimonialsDot').removeClass("w--current");
        $('#contestDot').removeClass("w--current");
    }

    var deadline = 'September 9 2016 23:59:59 GMT-0500';

    function getTimeRemaining(endtime){
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor( (t/1000) % 60 );
        var minutes = Math.floor( (t/1000/60) % 60 );
        var hours = Math.floor( (t/(1000*60*60)) % 24 );
        var days = Math.floor( t/(1000*60*60*24) );
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime){
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            if (t.total > 0) {
                daysSpan.innerHTML = t.days;
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
            }

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }

    initializeClock('clockdiv', deadline);

    //console.log(getTimeRemaining(deadline));

}());

$(document).on('click', '#btn-enter-contest', function(event){
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 800);
});


